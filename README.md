This todo app works exactly the same as other todo app. <br>
Create a new task to your to do list, and update them as you wish.<br>
Need to label which one is important? There is an important label for that. <br>
You can even filter your todo list by the importance and your completed one. <br>
Try it now :)
![](src/style/todo.png)